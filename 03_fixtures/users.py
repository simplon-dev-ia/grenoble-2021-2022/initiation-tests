import csv
import sqlite3


def load_csv(filepath: str, conn: sqlite3.Connection):
    with open(filepath, "r") as f:
        reader = csv.DictReader(f)
        rows = [(r["first_name"], r["last_name"], r["email"]) for r in reader]

        cur = conn.cursor()
        cur.executemany(
            """
            INSERT INTO users (first_name, last_name, email)
            VALUES (?, ?, ?)
            """,
            rows,
        )
        conn.commit()
