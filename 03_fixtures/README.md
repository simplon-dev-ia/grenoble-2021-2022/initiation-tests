# Les _fixtures_

Dans le domaine de la data, le code à tester intéragit très souvent avec l'environnement
extérieur (fichiers plats, APIs, bases de données, etc). Voyons comment définir
un contexte de test reproductible et déterministe pour les données.

Pour résoudre ce problème, la bibliothèque `pytest` met à disposition une fonctionalité
appelée [_fixtures_](https://docs.pytest.org/en/7.1.x/explanation/fixtures.html).

**Objectifs**

- Créer une fonction qui lit un fichier CSV et le charge dans une table d'une
base de données SQLite
- Chaque test doit pouvoir être exécuté indépendemment des autres afin de garantir
la reproductibilité
- Le(s) contexte(s) de test doivent être clairement définis

## Base de données

Dans ce dossier, 3 fichiers sont mis à votre disposition :

- [`users.py`](users.py)
- [`tests/test_users.py`](tests/test_users.py)
- [`tests/data/users.csv`](tests/data/users.csv)

Cet exemple est un début d'application qui permet de charger un fichier CSV dans une base de données SQLite.

- [ ] Lancer la suite de tests initiale avec `pytest` et vérifier que le test `test_load_csv_success` réussi
- [ ] Inspecter les 3 fichiers fournis et comprendre globalement comment le test fonctionne avec les _fixtures_

### Nombre d'utilisateurs

- [ ] Créer une nouvelle fonction qui compte le nombre d'utilisateur en base
- [ ] Ajouter une fixture permettant de remplir la base de données avec des utilisateurs fictifs
- [ ] Ajouter un test utilisant cette nouvelle fixture pour valider la nouvelle fonction

### Adresses e-mail

- [ ] Créer une fonction qui retourne toutes les adresses e-mail en base
- [ ] Ajouter une fixture permettant de remplir la base de données avec des utilisateurs fictifs (on peut réutiliser la fixture de la section précédente)
- [ ] Ajouter un test utilisant cette nouvelle fixture pour valider la nouvelle fonction

### Informations personnelles

- [ ] Créer une fonction qui retourne le nom et prénom d'un utilisateur à partir de son e-mail
- [ ] Ajouter une fixture permettant de remplir la base de données avec des utilisateurs fictifs (on peut réutiliser la fixture de la section précédente)
- [ ] Ajouter un test utilisant cette nouvelle fixture pour valider la nouvelle fonction
