import pathlib
import sqlite3
import pytest
import users


@pytest.fixture(scope="session")
def db():
    conn = sqlite3.connect(":memory:")
    yield conn
    conn.close()


@pytest.fixture(scope="function")
def users_table(db):
    cur = db.cursor()
    cur.execute(
        """
        CREATE TABLE users (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            first_name TEXT,
            last_name TEXT,
            email TEXT
        )
        """
    )
    db.commit()
    yield
    cur.execute("""DROP TABLE users""")
    db.commit()


def test_load_csv_success(db, users_table):
    filepath = pathlib.Path(__file__).parent / "data" / "users.csv"

    users.load_csv(filepath, db)

    cur = db.cursor()
    cur.execute("SELECT * FROM users")
    rows = cur.fetchall()
    assert len(rows) == 3
