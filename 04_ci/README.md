# Intégration continue

Les garanties fonctionnelles apportées par les tests n'ont de valeur uniquement
si tous les collaborateur d'un projet les utilisent. Un moyen de s'assurer que
les tests sont exécutés automatiquement à chaque nouvelle modification est
_l'intégration continue_. Dans sa forme la plus simple, c'est un processus
permettant d'exécuter les tests à chaque commit poussé sur un dépôt Git distant.

Nous utiliserons [GitLab-CI](https://docs.gitlab.com/ce/ci/) mais les concepts
sont bien sûr transférable avec d'autres systèmes (GitHub Actions, Jenkins, etc).

## GitLab-CI

Le premier bénéfice de la mise en place d'une intégration continue est la
validation du code du projet à chaque _push_ par les collaborateurs.

Commençons par créer un pipeline de CI simple :

- [ ] S'assurer que les tests de l'activité [`03_fixtures`](../03_fixtures/) fonctionnent en local
- [ ] **À la racine du dépôt Git**, créer le fichier configuration `.gitlab-ci.yml` avec le contenu suivant :
```yaml
stages:
  - test

test:
  image: python:3.9
  stage: test
  script:
    - pip install pipenv
    - pipenv install --dev --deploy
    - cd 03_fixtures
    - pipenv run pytest -v --junitxml=test-report.xml tests
  artifacts:
    reports:
      junit: 03_fixtures/test-report.xml
```
- [ ] Commiter et pousser sur GitLab
- [ ] Observer le résultat du pipeline de votre branche dans la section "CI/CD / Pipelines" du projet
- [ ] Observer le log du pipeline
- [ ] Vérifier que les résultats des tests sont remontés dans l'interface du pipeline

## GitLab Merge Request

Le second bénéfice de l'intégration continue est de faciliter la revue de code et la prise de décision
au moment d'intégrer de nouvelles modification dans la base de code d'un projet.

Simulons ce processus :

- [ ] Créer une _Merge Request_ pour demander la fusion de votre branche dans la branche `main`
- [ ] Vérifier que l'interface affiche bien le résultat du pipeline d'intégration continue
- [ ] Casser volontairement le code de la fonction `load_csv` (par exemple en chargeant partiellement le CSV en base)
- [ ] Commiter et pousser sur GitLab
- [ ] Vérifier que l'interface affiche bien que le pipeline de CI a échoué
- [ ] Rétablir le code, commiter et pousser

## Couverture de code

Pour terminer, nous pouvons donner toujours plus d'indications aux collaborateurs
du projet pour faciliter le travail de revue. Un indicateur intéressant (et controversé !)
est la _couverture de code_ : il mesure le pourcentage de lignes de code couvertes par
les tests. Il est également possible de contrôler la couverture des _branchements conditionnels_.

Le plugin [`pytest-cov`](https://pytest-cov.readthedocs.io) permet l'intégration de cette métrique avec Pytest.

Mettons cet indicateur en place :

- [ ] Vérifier que la couverture fonctionne pour l'activité [`03_fixtures`](../03_fixtures/) :
```bash
pytest -v --cov=. --cov-branch --cov-report=term-missing
```
- [ ] Modifier le fichier `.gitlab-ci.yml` en ajoutant la couverture de code à notre pipeline CI :
```yaml
stages:
  - test

test:
  image: python:3.9
  stage: test
  script:
    - pip install pipenv
    - pipenv install --dev --deploy
    - cd 03_fixtures
    - pipenv run pytest -v --cov=. --cov-branch --cov-report=term-missing --junitxml=test-report.xml tests
  coverage: /^TOTAL.+?(\d+\%)$/
  artifacts:
    reports:
      junit: 03_fixtures/test-report.xml
```
- [ ] Après validation du pipeline, vérifier que la métrique s'affiche bien dans la section "CI/CD / Jobs"
- [ ] Vérifier que la métrique s'affiche également dans la Merge Request de votre branche
- [ ] Ajouter une nouvelle fonction factice (par exemple `dummy_function` qui retourne le nombre 42), commiter, pousser
- [ ] Vérifier que la métrique diminue dans la Merge Request
- [ ] Ajouter un nouveau test pour cette nouvelle fonction
- [ ] Vérifier que la métrique remonte dans la Merge Request

Bravo, vous venez de terminer cette introduction à l'intégration continue !

Cela constitue un des **principaux fondamentaux** des pratiques modernes de développement
logiciel, permettant la **mise en place effective des pratiques Agiles et DevOps** !
