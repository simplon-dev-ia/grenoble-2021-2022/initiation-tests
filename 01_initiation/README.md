# Ma première suite de tests

**Objectif**

Pour cette première partie, on vous fournit un module Python contenant une fonction
ainsi qu'une suite de tests associée.

Lancer la suite de tests automatiques avec la commande suivante :

```bash
pytest -v tests
```

Vous venez d'exécuter votre première suite de tests unitaires !
Allez explorer les fichiers source Python suivants :

- [app.py](app.py)
- [tests/test_app.py](tests/test_app.py)

## TODO

- [ ] Que contient [app.py](app.py) ?
- [ ] Que contient [tests/test_app.py](tests/test_app.py) ?
- [ ] Changer le résultat de la fonction `stringify_number` en multipliant par 2 la valeur avant la conversion en chaine de caractère
- [ ] Que se passe-t-il si on ré-exécute la suite de tests unitaire ?
- [ ] Comment résoudre la situation ?
