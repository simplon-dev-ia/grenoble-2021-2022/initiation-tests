def stringify_number(value: int) -> str:
    if value < 0:
        raise ValueError()
    return str(value)
