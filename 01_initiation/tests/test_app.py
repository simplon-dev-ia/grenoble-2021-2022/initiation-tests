import pytest

from app import stringify_number


def test_stringify_number_success():
    assert stringify_number(42) == "42"


def test_stringify_number_failure():
    with pytest.raises(ValueError):
        stringify_number(-42)
