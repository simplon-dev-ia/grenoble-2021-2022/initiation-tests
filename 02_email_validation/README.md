# Validation d'e-mail

**Objectif**

- Créer une fonction `validate_email` qui valide le format d'une adresse e-mail
- La fonction `validate_email` a les contraintes suivantes :
    - Prendre en entrée une adresse e-mail sous forme de chaine de caractères
    - Retourner le résultat de validation sous forme de résultat booléen

## Implémentation initiale

L'implémentation initiale de `validate_email` doit valider uniquement le format `xxx@yyy`
sans utiliser d'expression régulière (uniquement de la manipulation de chaine de caractère).

- [ ] Créer le fichier `emails.py`
- [ ] Créer le fichier de test `tests/test_emails.py`
- [ ] Implémenter la fonction `validate_email` dans le module `emails.py`
- [ ] Implémenter et vérifier le test `test_validate_email_success` dans le module `tests/test_emails.py`
- [ ] Implémenter et vérifier le test `test_validate_email_failure` dans le module `tests/test_emails.py`

## Amélioration d'implémentation

Améliorons notre implémentation de la fonction `validate_email` pour valider le format `aaa@bbb.ccc`
en utilisant une expression régulière.

- [ ] Modifier l'implémentation de la fonction `validate_email`
- [ ] Vérifier la validité des tests `test_validate_email_success` et `test_validate_email_failure`
- [ ] Compléter les tests `test_validate_email_success` et `test_validate_email_failure` avec des nouveaux exemples pour le format complet

## Amélioration des tests

Les tests `test_validate_email_success` et `test_validate_email_failure` énumèrent
plusieurs exemples dans un même test, ce qui n'est pas une bonne pratique. Nous
devons être capable de valider indépendemment plusieurs exemples pour un même comportement.

La bibliothèque `pytest` permet de générer plusieurs versions d'un même test avec
des valeurs passées en paramètres : c'est la [_parametrisation_](https://docs.pytest.org/en/6.2.x/parametrize.html).

- [ ] Ré-écrire le test `test_validate_email_success` en le paramétrant pour plusieurs exemples
- [ ] Valider les tests paramétrés `test_validate_email_success[...]`
- [ ] Ré-écrire le test `test_validate_email_failure` en le paramétrant pour plusieurs exemples
- [ ] Valider les tests paramétrés `test_validate_email_failure[...]`
