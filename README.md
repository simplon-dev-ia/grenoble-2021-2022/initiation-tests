# Initiation aux tests

> Comment s'assurer que mon code fonctionne ?

## Recherche d'information

En groupe, rechercher des axes de réponses aux questions suivantes :

- [ ] Qu'est-ce qu'un _test unitaire_ ? Quel est son but ?
- [ ] Quels autres types de test existe-t-il en programmation ? Quelles sont les différences ?
- [ ] Quels outils permettent de tester du code Python ?
- [ ] En quoi les tests unitaires peuvent être utiles dans le domaine data / IA ?
- [ ] Comment fonctionne `pytest` dans les grandes lignes ?

## Mise en place

Dans un environnement virtuel Python, installer les bibliothèque Python [`pytest`](https://docs.pytest.org)
et [`pytest-cov`](https://pytest-cov.readthedocs.io).

Si vous utiliser `pipenv`, vous pouvez directement lancer `pipenv install -d` dans ce projet.

Vérifier que l'environnement est fonctionnel avec la commande suivante :

```bash
pytest --version
```
